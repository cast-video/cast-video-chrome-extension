import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

import { environment } from '../environment';

/**
 * Base class for websocket manipulations.
 */
export abstract class Websocket {

  /** JavaScript client for websocket emulation. */
  private readonly socket = new SockJS(environment.apiUrl);

  /** STOMP over WebSocket client. */
  private readonly client = Stomp.over(this.socket);

  /** Establishes the connection to the server. */
  connect() {
    this.client.connect({}, () => this.onConnect());
  }

  /** Disconnects the client. */
  disconnect() {
    this.client.disconnect(() => this.onDisconnect());
  }

  /** Connection hook. */
  protected onConnect() {}

  /** Disconnection hook. */
  protected onDisconnect() {}

  /**
   * Publishes to a room.
   * @template D - Type of the data.
   * @param {string} room
   * @param {D} [data]
   */
  protected publish<D>(room: string, data?: D) {

    this.client.send(room, data);
  }

  /**
   * Subscribes to a room.
   * @template D - Type of the data.
   * @param {string} room
   * @param callback
   */
  protected subscribe<D>(room: string, callback: (data: D) => Promise<void>) {

    this.client.subscribe(room, async ({ body }) => await callback(JSON.parse(body)));
  }
}
