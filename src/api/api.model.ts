export interface Video {
  /** YouTube identifier. */
  id: string;

  /** Title of the video. */
  title: string;

  /** Duration. */
  duration: string;
}

export interface PlayingVideo extends Video {

  /** Says if the video is paused. */
  paused: boolean;
}

export interface VideoPlayer {
  /** Video that is playing. */
  playing: PlayingVideo;

  /** Ordered videos that follow. */
  playlist: Video[];
}

export interface Actions {

  /** Force data fetching. */
  fetch();

  /** Add a video to the playlist. */
  addToPlaylist(id: string);

  /** Play the paused video. */
  play();

  /** Pauses the playing video. */
  pause();

  /** Plays the next video. */
  playNext();

  /** Go back in the playing video. */
  back30Seconds();

  /** Clears all the playlist. */
  clearPlaylist();
}
