import { Websocket } from './websocket';
import { VideoPlayer, Actions } from './api.model';

export class VideoWS extends Websocket implements Actions {

  constructor(private readonly dataFeed: (data: VideoPlayer) => Promise<void>) {
    super();
  }

  /** @override */
  fetch() {
    this.publish('/cast/fetch');
  }

  /** @override */
  addToPlaylist(id: string) {}

  /** @override */
  play() {}

  /** @override */
  pause() {}

  /** @override */
  playNext() {}

  /** @override */
  back30Seconds() {}

  /** @override */
  clearPlaylist() {}

  /** @override */
  protected onConnect() {
    this.subscribe('/topic/cast-data', this.dataFeed);
    
    this.fetch();
  }
}
