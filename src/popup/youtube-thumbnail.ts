import { Video } from '../api/api.model';

/**
 * Link generator for YouTube thumbnails.
 */
export class YouTubeThumbnail {

  /**
   * Finds a low quality thumbnail for the video.
   * @param {Video} video
   */
  static lowQuality({ id }: Video) {
    return `https://img.youtube.com/vi/${id}/default.jpg`;
  }

  /**
   * Finds a high quality thumbnail for the video.
   * @param video 
   */
  static highQuality({ id }: Video) {
    return `https://img.youtube.com/vi/${id}/hqdefault.jpg`;
  }
}
