export const deleteChildren = (parent: Node) => {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
};

export const eraseSelf = (self: Node) => self.parentNode.removeChild(self);

/** Get the YouTube ID of the current URL. */
export const getYouTubeID = async (): Promise<string | null> => new Promise(resolve => {

  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    let id = null;

    if (tabs.length > 0) {
      const { url } = tabs[0];
  
      if (url && url.length > 0) {
        const match = url.match(/^https\:\/\/www\.youtube\.com\/watch\?v=(.+)(?:\&list=.+)?$/);
  
        if (match && match.length === 2) {
          id = match[1];
        }
      }
    }

    resolve(id);
  });
});
