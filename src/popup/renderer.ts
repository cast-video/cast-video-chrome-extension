import { VideoPlayer, PlayingVideo, Video } from '../api/api.model';
import { deleteChildren, eraseSelf } from './utils';
import { YouTubeThumbnail } from './youtube-thumbnail';

/**
 * Data renderer.
 * @template T - Main data to render.
 */
export interface Renderer<T> {
  /** Data to render. */
  render(data: T);

  /** Renders the active tab informations. */
  renderActiveTab(id: string | null);

  /** Says if the server has been found or not. */
  renderServerState(state: boolean);

  /** Says if the page is loading or not. */
  renderLoading(loading: boolean);
}

/**
 * View controller.
 */
export class ViewRenderer implements Renderer<VideoPlayer> {

  /** Body of the page. */
  private static BODY: HTMLElement;

  /** Element that contains all the pending videos. */
  private static PENDING_VIDEOS_CONTAINER: Node;

  /** Graphical representation of a pending video. */
  private static PENDING_VIDEO_MODEL: HTMLElement;

  /** Graphical representation of the playing video. */
  private static VIDEO_PLAYER: HTMLElement;

  /** Graphical representation of the video adding proposal. */
  private static ADD_TO_PLAYLIST: HTMLElement;

  /** Wide graphical representation of the video proposal. */
  private static ADD_TO_PLAYLIST_WIDE: HTMLElement;

  /** References the templates. */
  static referenceTemplates() {
    ViewRenderer.BODY = document.querySelector('body');

    ViewRenderer.PENDING_VIDEOS_CONTAINER = document.querySelector('.pending-videos-list');
    ViewRenderer.PENDING_VIDEO_MODEL = document.querySelector('.pending-video');

    ViewRenderer.VIDEO_PLAYER = document.querySelector('.video-player');

    ViewRenderer.ADD_TO_PLAYLIST = document.querySelector('.add-to-playlist');
    ViewRenderer.ADD_TO_PLAYLIST_WIDE = document.querySelector('.share-video');

    ViewRenderer.clearPendingVideos();
  }

  /** Clears out the pending video container. */
  private static clearPendingVideos() {
    deleteChildren(ViewRenderer.PENDING_VIDEOS_CONTAINER);
  }

  /** Rendered data. */
  private renderedData: VideoPlayer;

  /** @inheritdoc */
  renderLoading(loading: boolean) {

    // TODO: Implement.
  }

  /** @inheritdoc */
  renderServerState(serverFound: boolean) {

    // TODO: Implement.
  }

  /** @inheritdoc */
  renderActiveTab(id: string | null) {

    // Check if the extension has at least something to display.
    if (this.renderedData.playlist === undefined && id === null) window.close();

    if (id === null || this.renderedData.playlist.find(e => e.id === id) !== undefined) {
      // TODO: Check if erase will not break on pulling refresh.
      eraseSelf(ViewRenderer.ADD_TO_PLAYLIST_WIDE);
      eraseSelf(ViewRenderer.ADD_TO_PLAYLIST);

      ViewRenderer.BODY.classList.remove('suggestion');
    } else {
      const style = `background-image: url('${YouTubeThumbnail.highQuality({ id } as Video)}');`;

      ViewRenderer.ADD_TO_PLAYLIST.querySelector('.video-thumbnail').setAttribute('style', style);
      ViewRenderer.ADD_TO_PLAYLIST_WIDE.setAttribute('style', style);
    }
  }

  /** Renders the video player. */
  render(data: VideoPlayer) {

    if (data.playing === undefined) {
      ViewRenderer.BODY.classList.add('share-only');

    } else {
      ViewRenderer.BODY.classList.remove('share-only');

      this.renderPlayingVideo(data.playing);
      this.renderPlaylist(data.playlist);
    }

    this.renderedData = data;
  }

  /**
   * Renders the playing video.
   * @param {PlayingVideo} playingVideo
   */
  private renderPlayingVideo(playingVideo: PlayingVideo) {

    const playingButton = ViewRenderer.VIDEO_PLAYER.querySelector('.main-action');

    playingVideo.paused
      ? playingButton.classList.remove('playing')
      : playingButton.classList.add('playing');

    ViewRenderer.VIDEO_PLAYER.setAttribute('style', 
      `background-image: url('${YouTubeThumbnail.highQuality(playingVideo)}');`);

    ViewRenderer.VIDEO_PLAYER.querySelector('.title p').innerHTML = playingVideo.title;
  }

  /**
   * Renders the playlist.
   * @param {Video[]} playlist
   */
  private renderPlaylist(playlist: Video[]) {

    ViewRenderer.clearPendingVideos();

    playlist.forEach(video => {

      const videoDOM = ViewRenderer.PENDING_VIDEO_MODEL.cloneNode(true) as HTMLElement;

      // Set the thumbnail.
      videoDOM.querySelector('.video-thumbnail').setAttribute('style',
        `background-image: url('${YouTubeThumbnail.lowQuality(video)}');`);

      // Set the title.
      videoDOM.querySelector('.video-title').innerHTML = video.title;

      // Set the duration.
      videoDOM.querySelector('.video-duration').innerHTML = video.duration;

      ViewRenderer.PENDING_VIDEOS_CONTAINER.appendChild(videoDOM);
    })
  }
}
