import { VideoPlayer } from '../api/api.model';
import { Renderer, ViewRenderer } from './renderer';
import { getYouTubeID } from './utils';
import { VideoWS } from '../api/video-ws';

/**
 * Main application class.
 */
class Application {

  /** Unique instance of the application. */
  private static _instance: Application;

  /** Always return the same instance of the application. */
  static get instance() {
    if (!Application._instance) Application._instance = new this();

    return Application._instance;
  }

  /** ID of the video suggestion. */
  private videoSuggestion: string;

  /** Main subscription to the webservice. */
  private readonly webservice = new VideoWS(async (data) => {

    if (this.videoSuggestion === undefined) {
      this.videoSuggestion = await getYouTubeID();
    }

    this.renderer.render(data);

    this.renderer.renderActiveTab(this.videoSuggestion);
  });

  /** Class that renders the data. */
  private readonly renderer: Renderer<VideoPlayer> = new ViewRenderer();

  private constructor() {}

  /** Application initialization. */
  init() {
    ViewRenderer.referenceTemplates();

    this.webservice.connect();
  }
}

/** Main entrypoint. */
document.addEventListener('DOMContentLoaded', async () => await Application.instance.init(), false);
